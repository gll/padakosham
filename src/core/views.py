from django.views import View
from django.http import HttpResponse
from django.shortcuts import render, redirect

from .models import Word

class Review(View):
    def get(self, request):
        unverified_words = Word.objects.filter(worddetails__is_verified=False)
        context = {
            'unverified_words':unverified_words
        }
        return render(request, "apps/core/review.html", context=context)


class EditWordView(View):
    def get(self, request, pk):
        return HttpResponse("LOL!")


class ApproveWordView(View):
    def post(self, request, pk):
        word = Word.objects.get(id=pk)
        word.worddetails.is_verified = True
        word.worddetails.verified_by = request.user
        word.worddetails.save()
        return redirect('review')
