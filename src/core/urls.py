from django.conf.urls import include, url

from .views import Review, EditWordView, ApproveWordView

urlpatterns = [
    url(r'^review/', Review.as_view(), name='review'),
    url(r'^word/(?P<pk>[0-9]+)/edit/$', EditWordView.as_view(), name='edit_word'),
    url(r'^word/(?P<pk>[0-9]+)/approve/$', ApproveWordView.as_view(), name='approve_word'),
]
