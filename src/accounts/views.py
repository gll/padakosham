from django.shortcuts import render

# Create your views here.
from registration.backends.simple.views import RegistrationView

class registration_redirect(RegistrationView):
    def get_success_url(self, request):
        return "/unaddstudent"
