from django.db import models


class Institution(models.Model):
    code = models.IntegerField()
    name = models.CharField(max_length = 100)
    level = models.CharField(max_length = 10)
    sub_district = models.ForeignKey('SubDistrict' ,  blank=True , null=True)
    address = models.CharField(max_length = 200)
    finance_type = models.CharField(max_length = 50)

    def __str__(self):
        return self.name

class SubDistrict(models.Model):
    name = models.CharField(max_length = 20 )
    edu_district = models.ForeignKey('EduDistrict' , blank=True , null=True)

    def __str__(self):
        return self.name

class EduDistrict(models.Model):
    name = models.CharField(max_length = 20 )
    district = models.ForeignKey('District' , blank=True , null=True)

    def __str__(self):
        return self.name


class District(models.Model):
    name = models.CharField(max_length = 20)

    def __str__(self):
        return self.name