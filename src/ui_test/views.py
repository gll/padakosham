from django.shortcuts import render

def login(request):
    return render(request, "apps/UI_test/login.html", )

def signup(request):
    return render(request, "apps/UI_test/signup.html", )

def add_student(request):
    return render(request, "apps/UI_test/add_student.html", )
